const util = require('util');

const {bot, botCommands} = require('../config');
const Messages = require('./messages');
const {getRandomIndex} = require('../utils/helper');
const {sendMessage} = require('../utils/telegramApiHelper');

module.exports.register = () => {
  bot.command(botCommands.go, (ctx) => {
    const timeRegex = {
      common: /^[0-9.:-]{1,5}$/g,
      withMinutes: /^\d{1,2}[:.\-]\d{2}$/,
      onlyHours: /^\d{1,2}$/
    };

    let time = ctx.message.text.substring(ctx.message.entities[0].length + 1).split(' ')[0];

    if (time.length > 0 && timeRegex.common.test(time)) {
      if (timeRegex.withMinutes.test(time)) {
        time = time.replace('.', ':').replace('-', ':');

        const timeParts = time.split(':');
        const hour = parseInt(timeParts[0]);
        const minute = parseInt(timeParts[1]);

        if (hour < 0 || hour > 23 || minute < 0 || minute > 59) time = '-1';
      } else if (timeRegex.onlyHours.test(time)) {
        time = (time >= 0 && time <= 23) ? `${time}:00` : '-1';
      } else {
        time = '-1';
      }
    } else {
      time = '-1';
    }

    if (time !== '-1') {
      sendMessage(ctx, util.format(Messages.commands.go.time, ctx.message.from.username, time), 'commands.go.time');
    } else {
      sendMessage(ctx, util.format(Messages.commands.go.general, ctx.message.from.username), 'commands.go.general');
    }
  });

  bot.command(botCommands.tea, ctx => {
    sendMessage(ctx, Messages.commands.tea[getRandomIndex(Messages.commands.tea.length)], 'commands.tea', true);
  });

  bot.command(botCommands.help, ctx => {
    sendMessage(ctx, Messages.commands.help, 'commands.help', true);
  });
};
