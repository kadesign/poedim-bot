const Extra = require('telegraf/extra');
const Logger = require('./logger');

module.exports.sendMessage = (ctx, msg, msgCode, markdown = false) => {
  ctx.telegram.sendMessage(ctx.message.chat.id, msg, (markdown ? Extra.markdown() : undefined))
    .then(() => {
      Logger.logSentMessage(ctx.message.chat.id, msgCode);
    });
};