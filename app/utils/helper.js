module.exports.to = function (promise) {
  return promise.then(data => {
    return [null, data];
  })
    .catch(err => [err]);
};

module.exports.getRandomIndex = arrayLength => {
  return Math.floor(Math.random() * arrayLength);
};

module.exports.fixUsername = username => {
  if (username.indexOf('_') >= 0) {
    username = username.replace(/_/g, '\\_');
  }

  return username;
};